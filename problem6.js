// problem6
// A buyer is interested in seeing only BMW and
//  Audi cars within the inventory.  Execute a function 
//  and return an array that only contains BMW and Audi cars.
//    Once you have the BMWAndAudi array,
//  use JSON.stringify() to show the results of the array in the console.

function problem6(inventory){
    let BMWAndAudi = []
    for (let i = 0; i < inventory.length; i++) {
        let carMod = inventory[i].car_make;
        if (carMod === 'BMW' || carMod === 'Audi') {
            BMWAndAudi.push(inventory[i]);
        }
    }
    console.log(JSON.stringify(BMWAndAudi))
}
module.exports=problem6
